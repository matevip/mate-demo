package vip.mate.core.security.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import vip.mate.core.api.Result;
import vip.mate.core.api.ResultCode;

/**
 * 异常处理器
 *
 * @author matevip
 * @since 2023/1/3
 */
@Slf4j
@RestControllerAdvice
public class ServerExceptionHandler {
    /**
     * 处理自定义异常
     */
    @ExceptionHandler(ServerException.class)
    public Result<String> handleException(ServerException ex) {

        return Result.fail(ex.getCode(), ex.getMsg());
    }

    /**
     * SpringMVC参数绑定，Validator校验不正确
     */
    @ExceptionHandler(BindException.class)
    public Result<String> bindException(BindException ex) {
        FieldError fieldError = ex.getFieldError();
        assert fieldError != null;
        return Result.fail(fieldError.getDefaultMessage());
    }

//    @ExceptionHandler(AccessDeniedException.class)
//    public Result<String> handleAccessDeniedException(Exception ex) {
//
//        return Result.fail(ResultCode.FORBIDDEN);
//    }

    @ExceptionHandler(Exception.class)
    public Result<String> handleException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return Result.fail(ResultCode.ERROR);
    }
}
