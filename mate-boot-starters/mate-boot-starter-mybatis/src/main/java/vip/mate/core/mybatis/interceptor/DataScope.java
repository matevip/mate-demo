package vip.mate.core.mybatis.interceptor;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * 数据权限
 *
 * @author pangu
 * @since 2023-1-1
 */
@Data
@RequiredArgsConstructor
public class DataScope {

    private String sqlFilter;

}
