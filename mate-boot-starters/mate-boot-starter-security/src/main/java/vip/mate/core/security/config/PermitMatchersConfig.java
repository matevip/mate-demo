package vip.mate.core.security.config;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 鉴权忽略地址配置类
 *
 * @author matevip
 * @since 2023/1/2
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "mate.security.ignore")
public class PermitMatchersConfig {

    private static final String[] SECURITY_ENDPOINTS = {
            "/actuator/**",
            "/v3/api-docs/**",
            "/swagger/**",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/swagger-ui/**",
            "/doc.html",
            "/webjars/**",
            "**/favicon.ico",
    };

    private List<String> urls = new ArrayList<>();

    /**
     * 首次加载合并ENDPOINTS
     */
    @PostConstruct
    public void initIgnoreSecurity() {
        Collections.addAll(urls, SECURITY_ENDPOINTS);
    }
}
