<p align="center">
  <img src="https://cdn.mate.vip/matecloud.svg" width="220">
</p>

[![个人信息](https://img.shields.io/badge/author-迈特云-blue.svg)](http://www.mate.vip/)
[![项目交流群](https://img.shields.io/badge/chat-项目交流群-green.svg)](https://jq.qq.com/?_wv=1027&k=oYxVM3uV)
![JDK Version](https://img.shields.io/badge/JAVA-JDK17+-red.svg)

[![Spring Boot](https://img.shields.io/maven-central/v/org.springframework.boot/spring-boot-dependencies.svg?label=Spring%20Boot&logo=Spring)](https://search.maven.org/artifact/org.springframework.boot/spring-boot-dependencies)  [![Spring Cloud](https://img.shields.io/maven-central/v/org.springframework.cloud/spring-cloud-dependencies.svg?label=Spring%20Cloud&logo=Spring)](https://search.maven.org/artifact/org.springframework.cloud/spring-cloud-dependencies)  [![Spring Cloud Alibaba](https://img.shields.io/maven-central/v/com.alibaba.cloud/spring-cloud-alibaba-dependencies.svg?label=Spring%20Cloud%20Alibaba&logo=Spring)](https://search.maven.org/artifact/com.alibaba.cloud/spring-cloud-alibaba-dependencies) 
