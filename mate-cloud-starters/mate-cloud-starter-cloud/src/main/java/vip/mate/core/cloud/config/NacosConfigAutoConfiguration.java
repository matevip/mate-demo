package vip.mate.core.cloud.config;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * Nacos配置自动配置
 * @author matevip
 */
@RefreshScope
@AutoConfiguration
public class NacosConfigAutoConfiguration {

}
