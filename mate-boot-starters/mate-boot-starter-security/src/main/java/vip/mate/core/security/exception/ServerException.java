package vip.mate.core.security.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.mate.core.api.ResultCode;

/**
 * 自定义异常
 *
 * @author matevip
 * @since 2023/1/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ServerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private int code;
    private String msg;

    public ServerException(String msg) {
        super(msg);
        this.code = ResultCode.ERROR.getCode();
        this.msg = msg;
    }

    public ServerException(ResultCode resultCode) {
        super(resultCode.getMsg());
        this.code = resultCode.getCode();
        this.msg = resultCode.getMsg();
    }

    public ServerException(String msg, Throwable e) {
        super(msg, e);
        this.code = ResultCode.ERROR.getCode();
        this.msg = msg;
    }

}
