package vip.mate.core.mybatis.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * dao 基类
 *
 * @author matevip
 * @since 2023/1/3
 */
public interface BaseDao<T> extends BaseMapper<T> {
}
