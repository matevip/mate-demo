package vip.mate.core.security.exception;

import cn.hutool.json.JSONUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import vip.mate.core.api.Result;
import vip.mate.core.api.ResultCode;

import java.io.IOException;

/**
 * todo
 *
 * @author matevip
 * @since 2023/1/2
 */
public class MateAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Origin", "Origin");
        response.getWriter().print(JSONUtil.toJsonStr(Result.fail(ResultCode.FORBIDDEN)));
    }
}
